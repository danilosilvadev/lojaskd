import { INCREMENT_WISHLIST, DECREMENT_WISHLIST, GET_LIST } from './actionsType'
import axios from 'axios'

const BASE_URL = 'https://raw.githubusercontent.com/'

export const increment = () => dispatch => {
  dispatch({
    type: INCREMENT_WISHLIST,
  })
};

export const decrement = () => dispatch => {
  dispatch({
    type: DECREMENT_WISHLIST,
  })
};

export const getList = () => dispatch =>
  axios.get(`${BASE_URL}danilosilvadev/mockljskd/master/mockljskd.json`)
    .then(response => dispatch({
      response: response.data,
      type: GET_LIST
    }))
    .catch(error => console.log(error))
