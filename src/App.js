import React, { Component } from 'react';
import styled from 'styled-components'
import Header from './components/Header'
import ProductsPage from './components/productsPage/ProductsPage'

const App = () => (
      <Container>
        <Header />
        <ProductsPage life='life'/>
      </Container>
    );

const Container = styled.div`
  font-size: medium;
  margin: 2rem;
`

export default App;
