import { shallow } from 'enzyme';
import { expect } from 'chai';
import mock from './mock'

import ProductsPage from '../components/productsPage';

describe('Finds if ProductsPage theres a mock prop', () => {

  it('renders a ProductsPage', () => {
    const wrapper = shallow(<ProductsPage />);
    expect(wrapper.prop('mock')).toEqual(expect.arrayContaining(mock));
  });
