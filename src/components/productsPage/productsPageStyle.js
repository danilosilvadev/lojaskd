export const gridView = {
  item: {
    listStyleType: 'none',
    margin: '.5rem'
  },

  itemSection: {
    width: '16rem',
    display: 'flex',
    flexDirection: 'column',
  },

  itemImage: {
    width: '220px'
  },

  itemSectionContent: {
    paddingTop: '1rem',
    paddingBottom: '1rem'
  },

  tag: {
    color: 'black',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    display: 'inline',
    backgroundColor: '#FFCA28',
    borderRadius: '50px'
  }
}

export const listView = {
  item: {
    listStyleType: 'none',
     margin: '1rem'
  },

  itemSection: {
    display: 'flex',
    alignItems: 'center'
  },

  itemImage: {
    width: '220px'
  },

  itemSectionContent: {
    paddingTop: '1rem',
    paddingBottom: '1rem',
    paddingLeft: '1rem'
  },

  content: {
    flexDirection: 'column'
  },

  tag: {
    color: 'black',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    display: 'inline',
    backgroundColor: '#FFCA28',
    borderRadius: '50px'
  }
}

//make this style work
export const CardView = {
  item: {
    listStyleType: 'none'
  },

  itemSection: {
    display: 'flex',
    alignItems: 'flex-start',
    flexDirection: 'column'
  },

  itemImage: {
    width: '440px'
  },

  itemSectionContent: {
    paddingTop: '1rem',
    paddingBottom: '1rem'
  },

  tag: {
    color: 'black',
    paddingLeft: '1rem',
    paddingRight: '1rem',
    display: 'inline',
    backgroundColor: '#FFCA28',
    borderRadius: '50px',
    position: 'relative',
    bottom: '37rem',
    left: '1rem',
    zIndex: '1'
  }
}
