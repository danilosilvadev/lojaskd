import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react'
import styled from 'styled-components'
import { connect } from "react-redux"
import PropTypes from 'prop-types';
import List from './products/List'
import { gridView, listView, CardView } from './productsPageStyle'
import { getList } from '../../actions/actions'

class ProductsPage extends Component{
  constructor(){
    super()
    this.handleView = this.handleView.bind(this)
    this.renderCard = this.renderCard.bind(this)
    this.renderList = this.renderList.bind(this)
    this.renderGrid = this.renderGrid.bind(this)
  }

  state = {
    view: 'grid',
    cardActive: false,
    listActive: false,
    gridActive: true
  }

  componentWillMount(){
    this.props.getList();
  }

  renderCard(){
    this.setState({cardActive: true, listActive: false, gridActive: false, view: 'card'})
  }

  renderList(){
    this.setState({cardActive: false, listActive: true, gridActive: false, view: 'list'})
  }

  renderGrid(){
    this.setState({cardActive: false, listActive: false, gridActive: true, view: 'grid'})
  }

  handleView(){
    const { view } = this.state
    const { mock } = this.props
    if(view === 'card'){
      return <List mock={mock} view={view} css={CardView} />
    } else if(view === 'list'){
      return <List mock={mock} view={view} css={listView} />
    } else {
      return <List mock={mock} view={view} css={gridView} />
    }
  }

  render(){

    const { cardActive, listActive, gridActive, view } = this.state

    return (
      <section>
        <div style={{marginTop: '1rem'}}>
          <Tag>Sou uma tag</Tag>
          <Tag>Sou outra tag</Tag>
        </div>
        <Header>
          <b>{this.props.mock.length} <br /> resultados</b>
          <Container>
            <ExibitionMode>Modo de exibição:</ExibitionMode>
              <a href='#' onClick={this.renderCard}>
                <Icon size='large' color={ cardActive ? 'blue' : 'grey'} name='block layout' />
              </a>
              <a href='#' onClick={this.renderList}>
                <Icon size='large' color={ listActive ? 'blue' : 'grey'} name='list layout' />
              </a>
              <a href='#' onClick={this.renderGrid}>
                <Icon size='large' color={ gridActive ? 'blue' : 'grey'} name='grid layout' />
              </a>
          </Container>
        </Header>
        <Ul grid={gridActive}>
          {this.handleView()}
        </Ul>
      </section>
    )
  }
}

const Tag = styled.div`
  color: white;
  margin-right: 1rem;
  padding-left: 1rem;
  padding-right: 1rem;
  font-size: .9rem;
  display: inline;
  background-color: #2185D0;
  border-radius: 50px;
`
const Header = styled.header`
 margin-top: 3rem;
 display: flex;
 justify-content: space-between;
`
const Container = styled.div`
  display: flex;
  align-items: center;
`
const ExibitionMode = styled.div`
  fontSize: .9rem;
  width: 5rem;
  color: #2185D0;
`
const Ul = styled.ul`
  display: flex;
  flex-wrap: wrap;
  flex-direction: ${props => props.grid ? 'row' : 'column'}
`


ProductsPage.propTypes = {
  mock: PropTypes.array.isRequired
};

export default connect(state => ({mock: state.getListReducer}), { getList })(ProductsPage);
