import React, { Component } from 'react';
import { Icon } from 'semantic-ui-react'
import styled from 'styled-components'
import { connect } from "react-redux";
import { increment, decrement } from '../../../actions/actions'

class Wish extends Component{
  constructor(){
    super()
    this.handleCounter = this.handleCounter.bind(this)
    this.handleView = this.handleView.bind(this)
  }
  state = {
    wish: false
  }

  positioners = {
    list: {
      left: '33rem',
      top: '4rem'
    },
    card: {
      left: '18rem',
      top: '4.5rem'
    },
    grid: {
      left: '10rem',
      top: '4.4rem',
      width: '3.6rem'
    }
  }

  handleView(){
    const { view } = this.props
    const { list, card, grid } = this.positioners
    switch(view){
    case 'list':
      return list
    case 'card':
      return card
    default:
      return grid
    }
  }

  handleCounter(){
    this.setState({wish: !this.state.wish},
    !this.state.wish ? this.props.increment(this.props.wishList) : this.props.decrement(this.props.wishList))
  }

  render(){
    return (
        this.props.view === 'card' ?
        <WishButtonLarge
          isActive={this.state.wish}
          onClick={this.handleCounter}
          style={this.handleView()}
          >
          <span style={{color: this.state.wish ? 'white' : 'grey', marginRight: '.5rem'}}>Add to Wishlist</span>
          <Icon style={{color: this.state.wish ? 'white' : 'grey'}} name='empty heart' size='large'/>
        </WishButtonLarge> :
        <WishButton
          isActive={this.state.wish}
          onClick={this.handleCounter}
          style={this.handleView()}
          >
          <Icon style={{color: this.state.wish ? 'white' : 'grey'}} name='empty heart' size='large'/>
        </WishButton>
    )
  }
}

const WishButton = styled.a`
  display: inline;
  border-radius: 100%;
  padding-top: 1rem;
  padding-bottom: 1rem;
  padding-left: .7rem;
  padding-right: .7rem;
  background-color: ${props => props.isActive ? '#0288D1' : 'white'};
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  position: relative;
  z-index: 1;
`

const WishButtonLarge = styled.a`
  color: black;
  padding 1rem;
  display: inline;
  background-color: #FFCA28;
  border-radius: 50px;
  background-color: ${props => props.isActive ? '#0288D1' : 'white'};
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  position: relative;
  z-index: 1;
`

export default connect(null, { increment, decrement })(Wish);
