import React, { Component } from 'react';
import image from '../../../assets/image.png'
import Wish from './Wish'

const List = props => (
          props.mock.map(item =>
          <li key={item.id} style={props.css.item}>
            <section style={props.css.itemSection}>
              <Wish view={props.view} />
                <img src={image} width={props.css.itemImage.width} />
               <section style={props.css.itemSectionContent}>
                 <header>
                   {item.product_name}
                 </header>
                 <br />
                   <article>
                     <b>{item.value} à vista</b><br />
                     <span> {item.value/10} em 10x s/ juros</span>
                   </article>
                   <br />
                   <a>
                     <div style={props.css.tag}>TAG %%</div>
                   </a>
                 </section>
           </section>
          </li>
         )
       )

export default List
