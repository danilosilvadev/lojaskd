import React from 'react';
import { Menu, Input, Icon } from 'semantic-ui-react'
import { connect } from "react-redux";
import styled from 'styled-components'

const Header = props => (<div>
  <Menu secondary size='massive' color='grey'>
    <Menu.Menu position="left">
      <a href='#'>
        <Icon size='large' color='grey' name='bars'/>
      </a>
    </Menu.Menu>
    <Menu.Menu position='right'>
      <div>
        <a href='#'>
        <Icon size='large' color='grey' name='heart'/>
        </a>
        <WishCounter href='#' counter={props.counter}>
          {props.counter}
        </WishCounter>
      </div>
      <a href='#'>
        <Icon color='grey' size='large' name='shopping cart'/>
      </a>
    </Menu.Menu>
  </Menu>

  <Input color='blue' icon='search' placeholder='Search...' fluid/>

</div>)

const WishCounter = styled.div`
  display: inline;
  font-weight: bold;
  text-decoration: none;
  font-size: .8rem;
  color: white;
  border-radius: 100%;
  padding-top: ${props => props.counter > 9 ? '.3rem' : '.1rem'};
  padding-bottom: ${props => props.counter > 9 ? '.3rem' : '.1rem'};
  padding-left: .4rem;
  padding-right: .4rem;
  background-color: #2185D0;
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
  position: relative;
  z-index: 1;
  right: 1rem;
  top: -.6rem;
`

export default connect(state => ({ counter: state.wishListReducer }))(Header)
