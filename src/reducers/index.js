import { combineReducers } from "redux";
import wishListReducer from './wishListReducer';
import getListReducer from './getListReducer';

export default combineReducers({
  wishListReducer,
  getListReducer
});
