import { INCREMENT_WISHLIST, DECREMENT_WISHLIST } from '../actions/actionsType'

export default function wishListReducer(state = 0, { type }) {
  switch(type) {
    case INCREMENT_WISHLIST:
      return state += 1
    case DECREMENT_WISHLIST:
      return state -= 1
    default:
      return state
  }
}
