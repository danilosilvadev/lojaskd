import { GET_LIST } from '../actions/actionsType'

export default function getListReducer(state = [], { type, response }) {
  switch(type) {
    case GET_LIST:
      return response
    default:
      return state
  }
}
